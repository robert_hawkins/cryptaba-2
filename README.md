# cryptaba 2.1 release
this is development release of cryptaba 2.0

# requires PHP5 SQLite3

# installation:
- cd to directory where you wish to install cryptaba
- run 'git clone https://bitbucket.org/crypt0in/cryptaba-2.git ./'
- edit application/config/default.config.php and rename it to config.php
- run 'cd application/db; php install_cryptaba.php'
- you are done! make sure your application/db/ directory is writable by server!
