default_style = '1337';
styles = ['crypt0chan', '1337', 'photon'];

function setStyle(style) {
    $.cookie('cryptaba_style', style, { expires: 365, path: '/' });
    $('link').each(function(){
        if (this.title === style) {
            this.disabled = false;
        } else if (this.title) {
            this.disabled = true;
        }
    });
}

function setPreferedStyle(){
    style = $.cookie('cryptaba_style');
    $('link').each(function(){
        if (this.title === style) {
            this.disabled = false;
        } else if (this.title) {
            this.disabled = true;
        }
    });
}

//extra cc javascript
function loadSiteOptions(){
    options_code = 
        '<div class="site-options-block"><p>Options:</p>'+
        '<form action="#options" class="options-form">'+
            '<p>Style <select name="style">';
    for (s in styles){
        options_code += '<option value="'+styles[s]+'">'+styles[s]+'</option>';
    }
    options_code += '</select></p>'+
            '<p><button>save</button></p>'+
        '</form>'+
        '</div>';
    $('.container').before(options_code);
}

function saveSiteOptions(options){
    setStyle(options.style.value);
}

$(document).ready(function(){
    $('.navigation-addition').append('<span id="show-site-options"><a href="#">[ options ]</a></span>');
    loadSiteOptions();
    $('#show-site-options').click(function(){
        $('.site-options-block').toggle();
    });
    
    $('form.options-form').submit(function(){
        saveSiteOptions(this);
        return false;
    });
    
    //check style cookie
    if ($.cookie('cryptaba_style') === null){
        $.cookie('cryptaba_style', default_style, { expires: 365, path: '/' });
    }
    //set style
    setPreferedStyle();
});