function escapeHtml(text) {
    return text
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
}

function markup(message) {
    //temp markup
    return message
            .replace(/\*\*([^(\*\*)]+)\*\*/g, "<b>$1</b>")
            .replace(/\*([^\*]+)\*/g, "<i>$1</i>")
            .replace(/`([^`]+)`/g, "<div>$1</div>")
            .replace(/\%\%([^\%\%]+)\%\%/g, "<span class=\"message-spoiler\">$1</span>")
//            .replace(/^&gt;([^\n|&gt;]+)$/gm, "<span class=\"message-quote\">&gt;$1</span>")
            .replace(/&gt;&gt;([0-9]+)/g, "<a class=\"reply-link\" href=\"#\">&gt;&gt;$1</a>")
            .replace(/\n/g, "<br>");
}

//function imageMarkup(message) {
//    return message
//            .replace(
//            /http:\/\/i\.imgur\.com\/([a-zA-Z0-9]+)\.(jpg|png|gif)/g,
//            "<small class=\"muted\">imgur:</small><br><img src=\"http:\/\/i.imgur.com\/$1.$2\" class=\"ext-image\" >"
//            )
//            .replace(
//            /http:\/\/([0-9a-z]+)\.tinypic\.com\/([a-zA-Z0-9]+)\.(jpg|png|gif)/g,
//            "<small class=\"muted\">tinypic:</small><br><img src=\"http:\/\/$1.tinypic.com\/$2.$3\" class=\"ext-image\" >"
//            );
//}
//
//function youtubeMarkup(message) {
//    return message
//            .replace(
//            /https?:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9-_]{11})/g,
//            "<div onclick=\"loadYouTube('$1');$(this).hide();\" id=\"yt$1\"><span class=\"youtube-link\">YouTube: $1 (click to watch)</span></div>"
//            );
//}
//
//function loadYouTube(id) {
//    ythtml = '<iframe width="560" height="315" src="//www.youtube-nocookie.com/embed/' + id + '?rel=0" frameborder="0" allowfullscreen></iframe>';
//    $(ythtml).insertAfter('#yt' + id);
//}

//achat ripped links
function openEx(url)
{
    w = window.open();
    w.document.write('<meta http-equiv="refresh" content="0;url=' + url + '">');
    w.document.close();
    return false;
}


//2ch ripped markup
function doAddTags(tag1, tag2, obj) {
    ToolbarTextarea = document.getElementById(obj);
    if (document.selection) {
        var sel = document.selection.createRange();
        sel.text = tag1 + sel.text + tag2;
    } else {
        var len = ToolbarTextarea.value.length;
        var start = ToolbarTextarea.selectionStart;
        var end = ToolbarTextarea.selectionEnd;
        var scrollTop = ToolbarTextarea.scrollTop;
        var scrollLeft = ToolbarTextarea.scrollLeft;
        var sel = ToolbarTextarea.value.substring(start, end);
        var rep = tag1 + sel + tag2;
        ToolbarTextarea.value = ToolbarTextarea.value.substring(0, start) + rep + ToolbarTextarea.value.substring(end, len);
        ToolbarTextarea.scrollTop = scrollTop;
        ToolbarTextarea.scrollLeft = scrollLeft;
        ToolbarTextarea.focus();
        ToolbarTextarea.setSelectionRange(start + tag1.length, end + tag1.length);
    }
}

//add markupButtons
function addMarkupButtons() {
    $('.markup-bar').html(
            '<span class="markup-item" onclick="doAddTags(\'**\',\'**\',\'textfield\')">[ bold ] </span>' +
            '<span class="markup-item" onclick="doAddTags(\'*\',\'*\',\'textfield\')">[ italic ]</span>' +
            '<span class="markup-item" onclick="doAddTags(\'%%\',\'%%\',\'textfield\')">[ spoiler ]</span>' +
            '<span class="markup-item" onclick="doAddTags(\'`\',\'`\',\'textfield\')">[ code ]</span>' +
            '<span class="markup-item" onclick="doAddTags(\'>\',\'\',\'textfield\')">[ quote ]</span>'+
            '<span class="markup-item" onclick="$(\'#enc_form\').toggle();">[ AES 256 ]</span>'+
            '<div id="enc_form" class="enc-form"><input type="text" placeholder="password" id="enc_pass" class="enc-form-pass">' +
            ' <span class="markup-item" id="enc_button">ENCRYPT</span></div>'
            );
}

function expandMenu() {
    $('#menucontent').toggle();
}

function togglePostform() {
    window.location.href = '#';
    $('#postform').toggle();
    setTimeout(function() {
        $('#textfield').focus()
    }, 100);

}

function expandPostform() {
    window.location.href = '#';
    $('#postform').show(100);
    setTimeout(function() {
        $('#textfield').focus()
    }, 100);

}

//Mousetrap.bind(['w', 'ц'], function() {
//    togglePostform();
//});
//Mousetrap.bind(['g g', 'п п'], function() {
//    window.location.href = '#';
//});
//Mousetrap.bind(['G', 'П'], function() {
//    window.location.href = '#bottom';
//});
//Mousetrap.bind(['r', 'к'], function() {
//    window.location.reload();
//});
//Mousetrap.bind(['b', 'и'], function() {
//    document.getElementById('goBack').click();
//    return false;
//});

function countNewMessages() {
    $.getJSON(rel_url + 'api/check_new_messages', function(json) {
        $('#new-messages').html('');
        if (json.number > 0 && json.number <= 50) {
            $('#new-messages').append(
                    '<span class="navigation-left-item">' +
                    '<a href="' + rel_url + 'board/lastvisit" title="click here to view new messages">[ ✉ ' + json.number + ' ]</a>' +
                    '</span>');
            $('title').html('[' + json.number + '] ' + site_name);
        } else if (json.number > 50) {
            $('#new-messages').append(
                    '<span class="navigation-left-item">' +
                    '<a href="' + rel_url + 'board/lastvisit" title="click here to view new messages">[ ✉ 50+ ]</a>' +
                    '</span>');
            $('title').html('[50+] ' + site_name);
        }

        if (json.sound == 'yes') {
            document.getElementById('message-alert').play();
        }
    });
}

function fillNewMessages() {
    $.getJSON(
            rel_url + 'api/check_new_messages', function(json) {
        $('#new-posts-container').empty();
        if (json.number === 0) {
            $('#new-posts-container').append(
                    '<div class="reply">No new messages, sir!</div>'
                    );
        } else {
            $(json.posts).each(function(index, value) {
                if (value.parent === '0') {
                    var parent_thread = value.id;
                    var add_text = 'new thread';
                } else {
                    var parent_thread = value.parent;
                    var add_text = 'reply to ' + value.parent;
                }
                var time = new Date(value.timestamp * 1000);
                $('#new-posts-container').append(
                        '<div class="reply" id="n' + value.id + '">' +
                        '<div class="reply-header"  style=\"font-weight: bold;\">' +
                        '<a href="' + rel_url + 'trd/' + parent_thread + '#n' + value.id + '">' +
                        '[' + value.boardname + ': ' + add_text + ']' +
                        '</a> ' +
                        '</div>' +
                        '<div class="reply-message">' + markup(escapeHtml(value.message)) + '</div>' +
                        '</div>'
                        );
            });
        }
    }
    );
}

function fillRecentMessages() {
    $.getJSON(
            rel_url + 'api/live_json/5', function(json) {
        $('#home-recent-messages').empty();

        $(json).each(function(index, value) {
            if (value.parent === '0') {
                var parent_thread = value.id;
                var add_text = 'new thread';
            } else {
                var parent_thread = value.parent;
                var add_text = 'reply to ' + value.parent;
            }
            var time = new Date(value.timestamp * 1000);
            $('#home-recent-messages').append(
                    '<div class="recent-message reply" id="n' + value.id + '">' +
                    '<div class="reply-header"  style=\"font-weight: bold;\">' +
                    '<a href="' + rel_url + 'trd/' + parent_thread + '#n' + value.id + '">' +
                    '[' + value.boardname + ': ' + add_text + ']' +
                    '</a> ' +
                    '</div>' +
                    '<div class="reply-message">' + markup(escapeHtml(value.message)) + '</div>' +
                    '</div>'
                    );
        });
    }

    );
}

function markRead() {
    $.get(rel_url + 'api/mark_read');
    setTimeout(function() {
        countNewMessages();
    }, 100);
    $('title').html(site_name);
}

function quickReply(id) {
    //to be removed
    var textarea = document.forms.postform.message;
    var text = ">>" + id;
    expandPostform();

    if (textarea) {
        if (textarea.createTextRange && textarea.caretPos) // IE
        {
            var caretPos = textarea.caretPos;
            caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == " " ? text + " " : text;
        }
        else if (textarea.setSelectionRange) // Firefox
        {
            var start = textarea.selectionStart;
            var end = textarea.selectionEnd;
            textarea.value = textarea.value.substr(0, start) + text + textarea.value.substr(end);
            textarea.setSelectionRange(start + text.length, start + text.length);
        }
        else
        {
            textarea.value += text + " ";
        }
        textarea.focus();
    }
}

$(document).ready(function() {
    //add code to navi
    $('.navigation-addition').append('<span id="new-messages" class="new-messages"></span>');
    //add sum k0d3z to teh end of page
    $('body').append('<audio id="message-alert"><source src="' + rel_url + 'img/sounds/alert.ogg" type="audio/ogg"></audio>');

    //code highlighting
    prettyPrint();
    //new messages
    countNewMessages();
    //check every 30 seconds
    setInterval(function() {
        countNewMessages();
    }, 30000);
    //add buttons
    addMarkupButtons();
    //encryptor markup
    $('#enc_button').click(function(){
        message = document.getElementById('textfield').value;
        password = document.getElementById('enc_pass').value;
        encrypted = CryptoJS.AES.encrypt(message, password);
        $('#textfield').val('#AES '+encrypted+'#');
    });
    
    //ctrl+enter posting
    $("#textfield").keyup(function(e) {
        if ((e.ctrlKey || e.metaKey) && (e.keyCode == 10 || e.keyCode == 13)) {
            $("#submitButton").click();
        }
    });

    //on top scrolling
    $("#onTop").click(function() {
        $("html, body").animate({scrollTop: 0}, "fast");
        return false;
    });

    $('#postform').hide();
    
    //client side markup
    $('div.reply-message, div.thread-message').each(function() {
        message = $(this).html();
        message = message
                .replace(
                    /(https?:\/\/)([a-zA-Z0-9-_.]{2,255})(:[0-9]+|)([a-zA-Z0-9-_.?=#%/&;]{0,})/g,
                    "<span class=\"ext-link\" title=\"safe url\" onclick=\"openEx('$1$2$3$4')\">$1$2$3$4</span>"
                )
                .replace(
                /#AES ([A-Za-z0-9=/+]+)#/, 
                "<div class=\"cyph3r-block\">✱ Encrypted message"+
                    "<form action=\"#decrypt\" class=\"decrypt-form\">"+
                        "<input type=\"hidden\" name=\"text\" value=\"$1\">"+
                        "<input type=\"text\" name=\"password\" class=\"enc-form-pass\"> <button class=\"markup-item\">decrypt</button>"+
                    "</form>"+
                "</div>")
                .replace(/:idiot:/, "<span class=\"idiot\"> Я ИДИОТ!!1 <\/span>");
        
        $(this).html(message);
        
    });
    //click to show decrypt form
    $('div.cyph3r-block').click(function(){
        $(this).find('form').show();
    });
    //decrypt messages
    $('form.decrypt-form').submit(function(){
        cyph3r = this.text.value;
        password = this.password.value;
        decrypted = CryptoJS.AES.decrypt(cyph3r, password);
        $(this).html(escapeHtml(decrypted.toString(CryptoJS.enc.Utf8)));
        setTimeout(function(){$(this).focus();}, 100);
    });
    
    //reply link preview
    $('a.reply-link').mouseover(function(){
        anch = '#n'+this.title;
        $(anch).css('box-shadow', 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 5px green');
        setTimeout(function(){
            $(anch).css('box-shadow', 'inset 0 0px 0px rgba(0, 0, 0, 0.075), 0 0 0px green');
        }, 5000);
    });
});


