<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//temp die
exit('currently in development');

class Userspace extends CI_Controller {
    /*
     * This is users controller, that will do all user's stuff (register, edit settings, boards)
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model( array('posts', 'boards', 'users') );
        
    }

    function login_form() {
        $data['visible_boards'] = $this->boards->visible_boards_info();
        
        $this->load->view('header', $data);
        $this->load->view('navbar', $data);
        $this->load->view('userspace/login_form');
        $this->load->view('footer'); 
    }
    
    function login(){
        //logs user in (if all correct)
        //if(empty($this->input->post('username')) || empty($this->input->post('password'))) show_error('no username or password');
        //get vars
        $username=$this->input->post('username');
        $password=$this->input->post('password');
        //check valid
        $id = $this->users->valid_login($username, $password);
        $hash = md5($password);
        
        $this->input->set_cookie('logged_in', '1', 31536000, $this->input->server('SERVER_NAME'));
        $this->input->set_cookie('user_id', $id, 31536000, $this->input->server('SERVER_NAME'));
        $this->input->set_cookie('hash', $hash, 31536000, $this->input->server('SERVER_NAME'));
        redirect(rel_url('userspace/user_zone'));
    }
    
    function logout() {
        //logs user off
    }
    
    function register(){
        //register new user
    }
    
    function user_zone() {
        // edit user settings
        echo $this->input->cookie('user_id');
    }
    
    function board_zone() {
        //create or edit user boards
        // no moderation here
    }
    
    function root() {
        //super user space
        // here he can edit any post/board/user and global settings
    }
}
