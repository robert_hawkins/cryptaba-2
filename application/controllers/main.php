<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
    /*
     * This is a main controller, that will display all navigation and info stuff
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model( array('posts', 'boards') );
        
    }
    
    function index() {
        //will show home of board - statistics and navigation

        $data_stats['visible_boards'] = $this->boards->visible_boards_info();
        $data_stats['overall_stats'] = $this->boards->overall_stats();

        $this->load->view('header', $data_stats);
        $this->load->view('navbar', $data_stats);
        $this->load->view('main/main_index', $data_stats); 
        $this->load->view('footer'); 
    }
    
    function root() {
        //edit everything in DB like a boss
    }
    
    function site_settings() {
        if ($this->input->post('set_images')){
            if ($this->input->post('set_images') == 'yes') {
                $this->input->set_cookie('load_images', 1, 31536000, $this->input->server('SERVER_NAME'));
            } elseif ($this->input->post('set_images') == 'no') {
                $this->input->set_cookie('load_images', 0, 0, $this->input->server('SERVER_NAME'));
            }
        }
        if ($this->input->post('set_youtube')){
            if ($this->input->post('set_youtube') == 'yes') {
                $this->input->set_cookie('load_youtube', 1, 31536000, $this->input->server('SERVER_NAME'));
            } elseif ($this->input->post('set_youtube') == 'no') {
                $this->input->set_cookie('load_youtube', 0, 0, $this->input->server('SERVER_NAME'));
            }
        }
        $data_stats['visible_boards'] = $this->boards->visible_boards_info();
        
        $this->load->view('header', $data_stats);
        $this->load->view('navbar', $data_stats);
        $this->load->view('main/site_settings');
        $this->load->view('footer'); 

    }
    function main_help() {
        $data_stats['visible_boards'] = $this->boards->visible_boards_info();
        
        $this->load->view('header', $data_stats);
        $this->load->view('navbar', $data_stats);
        $this->load->view('main/main_help');
        $this->load->view('footer'); 

    }
}
