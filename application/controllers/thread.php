<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thread extends CI_Controller {
    /*
     * This is a thread controller, that will display discussions (replies by id)
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model( array('posts', 'boards') );
        
    }
    
    function show() {
        //show post(thread) by id
        //show OP post + posts where parent == id of OP post

        //get variables
        
        $threadid = $this->uri->segment(2);
        if (!$threadid) {
            show_error('no thread specified');
        }
        
        $this->posts->valid_thread($threadid);
        $boardid = $this->posts->boardid_by_postid($threadid);
        $boardname = $this->boards->name_by_id($boardid);

        
        $data_thread['posts_id'] = $this->posts->id_posts($boardid, $threadid);
        $data_thread['boardid'] = $boardid;
        $data_thread['boardname'] = $boardname;
        $data_thread['threadid'] = $threadid;
        $data_thread['visible_boards'] = $this->boards->visible_boards_info();

        //load views
        $this->load->view('header', $data_thread);
        $this->load->view('navbar', $data_thread);
        $this->load->view('menu', $data_thread);
        $this->load->view('postform', $data_thread);
        //$this->load->view('content/content_id', $data_thread);
        $this->load->view('thread/thread_show', $data_thread);

        $this->load->view('footer');   
    }
    
    
}
