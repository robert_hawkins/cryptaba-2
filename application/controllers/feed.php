<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feed extends CI_Controller {
    /*
     * Feed controller!
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model( array('posts', 'boards') );
        
    }
    
    function all() {
        //feed of visible boards
        $data_feed['feed'] = $this->posts->get_all_feed();
        $this->load->view('feed/feed_all', $data_feed);
    }
    
    function board() {
        //feed of board (ex: feed/board/general)

        $boardname = $this->uri->segment(3);
        if (!$boardname) {
            show_error('no board specified');
        }
        $this->boards->valid_board_name($boardname);
        
        $boardid = $this->boards->id_by_name($boardname);
        $data_feed['boardname'] = $boardname;
        
        $data_feed['feed'] = $this->posts->get_board_feed($boardid);
        $this->load->view('feed/feed_board', $data_feed);
    }
    
    function thread() {
        //feed of thread (ex: feed/thread/100)
        // need to do lot of work here! views!
        
        $threadid = $this->uri->segment(3);
        $this->posts->valid_thread($threadid);
        $boardid = $this->posts->boardid_by_postid($threadid);
        $boardname = $this->boards->name_by_id($boardid);

        //get thread feed

        $data_feed['boardname'] = $boardname;            
        $data_feed['threadid'] = $threadid;
        $data_feed['feed'] = $this->posts->get_thread_feed($threadid);
        $this->load->view('feed/feed_thread', $data_feed);        
    }
    
    
}
