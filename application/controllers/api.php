<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {
    /*
     * API controller
     */

    public function __construct() {
        parent::__construct();
        $this->load->model(array('posts', 'boards'));
    }

    function index() {
        //$this->load->view('api');
        echo 'api controller!';
    }

    function insert_postform() {
        // regular insert posts 
        if (!$this->config->item('allow_posting'))
            show_error('posting disabled');

        //get input
        $data = $this->input->post();
        // error checks
        if (!isset($data['message']))
            show_error('empty message');
        if (!isset($data['parent']))
            show_error('parent not set');
        if (!isset($data['boardid'])) show_error('board not set');

        if ($data['parent'] == '0' && !isset($data['boardid']))
            show_error('boardid not set');
        //flood check
        $this->posts->check_flood(1);
        //check if thread and board is valid
        if (isset($data['boardid']))
            $this->boards->valid_board_id($data['boardid']);
        if ($data['parent'] != '0')
            $this->posts->valid_thread($data['parent']);
        //filter message
        $data['message'] = $this->messages->validate_message($data['message']);
        //bump thread
        if ($data['parent'] != '0')
            $this->posts->bump_thread($data['parent']);
        //insert message into database and return thread id
        $result = $this->posts->insert($data);

        // set cookies (mark all as read)
        $this->input->set_cookie('lastvisit', time(), 31536000, $this->input->server('SERVER_NAME'));
        $this->input->set_cookie('lastnumber', 0, 0, $this->input->server('SERVER_NAME'));
        //redirect 
        $redir_url = rel_url('trd/' . $result);
        $this->output->set_header('Location: ' . $redir_url);
    }
    
    function insert() {
        // api for inserting posts
        if (!$this->config->item('allow_posting')) show_error('posting disabled');
        //get input
        $post = $this->input->post();
        if (empty($post['data'])) show_error('no data specified');
        //decode to array
        $data = json_decode($post['data'], true);
        // error checks
        if (!isset($data['message'])) show_error('empty message');
        if (!isset($data['parent'])) show_error('parent not set');
        if (!isset($data['board'])) show_error('board not set');
        $this->boards->valid_board_name($data['board']);
        $data['boardid'] = $this->boards->id_by_name($data['board']);
        if ($data['parent'] == '0' && !isset($data['boardid'])) show_error('boardid not set');
        //flood check
        $this->posts->check_flood(1);
        //check if thread and board is valid
        if (isset($data['boardid'])) $this->boards->valid_board_id($data['boardid']);
        if ($data['parent'] != '0') $this->posts->valid_thread($data['parent']);
        //filter message
        $data['message'] = $this->messages->validate_message($data['message']);
        //bump thread
        if ($data['parent'] != '0') $this->posts->bump_thread($data['parent']);
        //insert message into database and return thread id
        $result = $this->posts->insert($data);
        $output = array('post' => $result, 'rel_url' => rel_url('trd/' . $result), 'abs_url' => site_url('trd/'.$result));
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($output));
    }

    public function check_new_messages() {
        // will set lastvisit, lastnumber cookie (if not set) and return number of ommited messages, messages content
        // and play sound or not
        $lastvisit = $this->input->cookie('lastvisit');
        $lastnumber = $this->input->cookie('lastnumber');
        //set lastvisit cookie if it wasnt set
        if (!$lastvisit)
            $lastvisit = $this->input->set_cookie('lastvisit', time(), 31536000, $this->input->server('SERVER_NAME'));
        // get lastvisit messages
        $messages = $this->posts->lastvisit_posts($lastvisit);
        //set lastnumber cookie if it wasnt set
        if (!$lastnumber)
            $lastnumber = $this->input->set_cookie('lastnumber', $messages->num_rows(), 0, $this->input->server('SERVER_NAME'));

        //check if lastnumber is less than messages - do alarm sound :3
        if ($lastnumber < $messages->num_rows()) {
            $sound = 'yes';
            $this->input->set_cookie('lastnumber', $messages->num_rows(), 0, $this->input->server('SERVER_NAME'));
        } else {
            $sound = 'no';
        }

        //result array to encode
        $result = array('number' => $messages->num_rows(), 'posts' => $messages->result(), 'sound' => $sound);

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }

    public function mark_read() {
        // will set new cookie
        $this->input->set_cookie('lastvisit', time(), 31536000, $this->input->server('SERVER_NAME'));
        $this->input->set_cookie('lastnumber', 0, 0, $this->input->server('SERVER_NAME'));
        return TRUE;
    }

    public function live_raw() {
        //will return raw last 1-100 messages on visible boards
        // for curl and other custom apps

        $number = $this->uri->segment(3);
        $messages = $this->posts->live($number);
        $raw_data = "";

        foreach ($messages->result() as $row) {
            $raw_data .= "[" . $row->boardname . "] " .
                    "#" . $row->id . " " .
                    $row->message . "\r\n";
        }

        $this->output
                ->set_header('Content-Type: text/plain; charset=utf-8')
                ->set_output($raw_data);
    }

    public function live_json() {
        // will return last n posts on visible boards. n - GET value
        $number = $this->uri->segment(3);
        $messages = $this->posts->live($number);
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($messages->result()));
    }

    public function check_new() {
        // check for new messages from timestamp (with board name optional)
        $time = $this->input->post('timestamp');
        if (empty($time)) show_error('No timestamp specified');
        $board = $this->uri->segment(3, "");
        $messages = $this->posts->lastvisit_posts($time, $board);
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($messages->result()));
    }

}
