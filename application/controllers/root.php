<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Root extends CI_Controller {
    /*
     * This is root controller. Global admin interface
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model( array('posts', 'boards') );
        session_start();
    }
    
    function index() {
        $this->load->view('root/login');
    }
    
    function login() {
        if ( !$this->input->post('password') ) show_error("no password");
        if ($this->input->post('password') == $this->config->item('r00t_password')) {
            $_SESSION['is_root'] = "yes";
            redirect('root/r00t_panel');
        } else {
            show_error('invalid password');
        }
    }
    
    function logout() {
        $_SESSION['is_root'] = "";
        session_destroy();
        redirect('root/index');
    } 

    function r00t_panel() {
        //index of r00t panel
        if (!isset($_SESSION['is_root']) || $_SESSION['is_root'] != "yes") show_error('you are not root!');

        $this->load->view('root/panel');
    }
    
    function del_post() {
        //delete post by id
        if (!isset($_SESSION['is_root']) || $_SESSION['is_root'] != "yes") show_error('you are not root!');
        if (!$this->input->post('postid')) show_error('no postid');
        
        $this->posts->delete_post($this->input->post('postid'));
        redirect('root/r00t_panel');
    }

    function del_post_range() {
        //delete post by id
        if (!isset($_SESSION['is_root']) || $_SESSION['is_root'] != "yes") show_error('you are not root!');
        if (!$this->input->post('id_start') || !$this->input->post('id_end')) show_error('no postid');
        
        $this->posts->delete_range($this->input->post('id_start'), $this->input->post('id_end'));
        redirect('root/r00t_panel');
    }
    
    function add_board() {
        if (!isset($_SESSION['is_root']) || $_SESSION['is_root'] != "yes") show_error('you are not root!');
        $data = array(
            'name' => $this->input->post('name'),
            'visible' => $this->input->post('visible'),
            'descr' => $this->input->post('descr')
        );
        $this->boards->add_board($data);
        redirect('root/r00t_panel');
    }

    function change_motd() {
        if (!isset($_SESSION['is_root']) || $_SESSION['is_root'] != "yes") show_error('you are not root!');
        $data = array(
            'name' => $this->input->post('name'),
            'motd' => $this->input->post('motd')
        );
        $this->boards->change_motd($data);
        redirect('root/r00t_panel');
    }
    
    function delete_board() {
        if (!isset($_SESSION['is_root']) || $_SESSION['is_root'] != "yes") show_error('you are not root!');
        $id = $this->boards->id_by_name($this->input->post('name'));
        $this->boards->delete_board($id);
        redirect('root/r00t_panel');
    }

}
