<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board extends CI_Controller {
    /*
     * This is a boards controller. Used to display boards, live board and recent messages (last timestamp)
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model( array('posts', 'boards') );
        
    }
    
    function show() {
        //show index of any board
        //load models
 
        $this->load->library('pagination');
        
        //get variables
        $boardname = $this->uri->segment(2);
        $offset = $this->uri->segment(3, "1");
        
        //check if board is invalid
        $this->boards->valid_board_name($boardname);
        
        //variables
        $boardid = $this->boards->id_by_name($boardname);
        $data_board['board_info'] = $this->boards->board_info($boardid);
        $data_board['posts_index'] = $this->posts->index_posts($boardid, $offset);
        $data_board['boardid'] = $boardid;
        $data_board['boardname'] = $boardname;
        $data_board['visible_boards'] = $this->boards->visible_boards_info();
        
        //pagination config
        $pagination['base_url'] = rel_url('brd/' . $boardname);
        $pagination['total_rows'] = $data_board['board_info']['threads_num'];
        $pagination['per_page'] = 10;
        $pagination['use_page_numbers'] = TRUE;
        $pagination['uri_segment'] = 3;
        $this->pagination->initialize($pagination);
        //variable pagination
        $data_board['pagination'] = $this->pagination->create_links();

        //load views
        $this->load->view('header', $data_board);
        $this->load->view('navbar', $data_board);
        $this->load->view('menu', $data_board);
        $this->load->view('postform', $data_board);
        $this->load->view('board/board_show', $data_board);
        $this->load->view('footer');
    }
    
    function lastvisit(){
        //messages from timestamp
        // will be like twitter
        
        $time = $this->input->cookie('lastvisit');
        if (!$time) show_error('no cookie');
        $data_lastvisit['visible_boards'] = $this->boards->visible_boards_info();
        //$data_lastvisit['overall_stats'] = $this->boards->overall_stats();
        $data_lastvisit['posts_lastvisit'] = $this->posts->lastvisit_posts($time);
        
        //load views
        $this->load->view('header', $data_lastvisit);
        $this->load->view('navbar', $data_lastvisit);
        $this->load->view('menu', $data_lastvisit);
        $this->load->view('board/board_lastvisit', $data_lastvisit);
        $this->load->view('footer');
        
    }
    
    function mod() {
        //edit board like a mod
    }
    

}
