<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Messages {

    function validate_message($message) {
        //message filters
        if (strlen($message) > 8000) {
            show_error('your message is longer than 8000 symbols!');
        }
        if (strlen($message) < 1) {
            show_error('please, enter your message!');
        }
        $message = preg_replace("/((\\n){2,})/","\n\n", $message);
        //return message
        return rtrim($message);
    }
    
    function markup($message){
        //basic wakaba style markup + links in thread + convert \n to <br>'s
        $message = htmlspecialchars($message);
        
        $search = array(
                "/\*\*([^(\*\*)]+)\*\*/",
                "/\*([^\*]+)\*/",
                "/\%\%([^\%\%]+)\%\%/",
                "/^(&gt;(?!&gt;)([^\n]+))/m",
                "/&gt;&gt;([0-9]+)/",
                "/`([^`]+)`/"
        );
        
        $replace = array(
                "<b>$1</b>",
                "<i>$1</i>",
                "<span class=\"message-spoiler\">$1</span>",
                "<span class=\"message-quote\">$1</span>",
                "<a class=\"reply-link\" title=\"$1\" href=\"#n$1\">&gt;&gt;$1</a>",
                "<pre class=\"prettyprint\">$1</pre>"
        );
        
        $message = preg_replace($search, $replace, $message);
        
        // \n to <br>
        $message = str_replace("\n", "<br>", $message);
        
        return $message;
    }
    
    function clean_markup($message) {
        //will delete all markup for use in the home page and feed (?)
        
        $message = htmlspecialchars(rtrim($message));
        
        return $message;
    }
}
