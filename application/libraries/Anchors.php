<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Anchors {
    
   function board_anchor($board, $text) {
        //creates anchor for board
       $result = '<a href="' . rel_url('brd') . '/' . $board . '">' . $text . '</a>';
       return $result;
    }

   function thread_anchor($board, $threadid, $text) {
        //creates anchor for board
       $result = '<a href="' . rel_url('trd') . '/' . $threadid . '">' . $text . '</a>';
       return $result;
    }
}
?>