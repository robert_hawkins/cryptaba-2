<?php

class Users extends CI_Model {
    
    public function valid_login($username, $password){
        //if username and password is valid - return user id
        if(preg_match('/^0-9a-z/', $username)) show_error('invalid characters in username');
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $result = $this->db->get();
        
        if ($result->num_rows() != 1) {
            show_error('invalid login details');
        } else {
            return $result->row()->id;
        }
        
    }
    
    public function check_login(){
        //check cookies if logged in and if valid user
    }
    
    public function user_by_id($userid) {
        //return user info by id

    }
    
    public function create_user($user_info) {
        //create new user
    }
    
    public function delete_user($userid) {
        //delete user
    }
    
    public function is_root($user_info) {
        //check if user is root
        if ($user_info['id'] == $this->config->item('root_userid')) {
            return true;
        } else {
            show_error('you are not r00t');
        }
    }
    
    public function update_user_info() {
        //update username or other info
    }
    
    
}