<?php

class Posts extends CI_Model {
    
    function valid_thread($threadid){
        //if thread exists - return true, else - false
        if (preg_match('/[^0-9]/', $threadid) == true) {
            show_error('thread id contains nonintegers and other shit');
        } else {
            $this->db->from('posts');
            $this->db->where('id', $threadid);
            $result = $this->db->get();
            if ($result->num_rows() > 0) {
                $final = $result->row();
                if ($final->parent == 0){
                    return TRUE; # everything is OK!!!
                } else {
                    show_error ('parent is invalid!');
                }
            } else {
                show_error('thread does not exist.');
            }
        }
    }
    
    function id_posts($boardid, $threadid) {
        //return posts of single thread
        // TO DO: no board id anymore
        $this->db->from('posts');
        $this->db->where('boardid', $boardid);
        $this->db->where('id', $threadid);
        $this->db->or_where('parent', $threadid);
        $this->db->order_by('id');
        return $this->db->get();
    }
    
    function index_posts($boardid, $offset) {
        // return posts to use on index of board
        if (preg_match('/[^0-9]/', $offset) == true) {
            show_error('what are you doing, nigga???'); # offset contains nonintegers and other shit
        }
        
        $limit = 10;
        $startlimit = ($offset * 10) - 10;

        $this->db->from('posts');
        $this->db->where('parent', '0');
        $this->db->where('boardid', $boardid);
        $this->db->order_by('bumped', 'desc');
        $this->db->limit($limit, $startlimit);

        return $this->db->get(); # need add return only if got posts or offset is default (board empty)
    }
    
    function lastvisit_posts($time, $board = '') {
        //return posts from last visit
        if (preg_match('/[^0-9]/', $time) == true)
            show_error('what are you doing, nigga???');

        $this->db->select('posts.id, parent, timestamp, message, boardname');
        $this->db->from('boards');
        $this->db->join('posts', 'posts.boardid = boards.id');
        $this->db->where('visible', '1');
        $this->db->where('timestamp >', $time);
        if (!empty($board)) {
            if (preg_match('/[^0-9a-z]/', $board) == true) show_error('what are you doing, nigga???');
            $this->db->where('boardname', $board);
        }
        $this->db->order_by('timestamp', 'desc');
        $this->db->limit(51);
        return $this->db->get();
    }
    
    function check_flood($id) {
        //here flood control
        // all values hardcoded, 255 posts daily (floodlimit in development)
        $day = time() - 60 * 60 * 24;
        
        $this->db->from('posts');
        $this->db->where('timestamp >', $day);
        $result = $this->db->get();
        if ($result->num_rows() >= 255) {
            show_error('flood or wipe detected. please, chill out.');
        }
    }
    
    function insert($data){
        //insert post
        // TO DO: no boardid for replies
        $clean_data = array(
            'message' => $data['message'],
            'parent' => $data['parent'],
            'timestamp' => time(),
            'bumped' => time(),
            'boardid' => $data['boardid'],
            'userid' => '1'
        );
        $table = 'posts';
        
        $this->db->insert($table, $clean_data);
        //return thread id
        if ($data['parent'] != '0'){
            $result = $data['parent'];
        } else {
            $result = $this->db->insert_id();
        }
        return $result;
    }
    
    function delete_post($id){
        //delete single post (and it's replies if any)
        if (preg_match('/[^0-9]/', $id) == true) show_error('wtf bro');
        //check if post exists
        $check = $this->db->from('posts')->where('id', $id)->get();
        if ($check->num_rows() == 0) show_error('nothing to delete, sir!');
        //delete action
        $this->db->where('id', $id)->or_where('parent', $id)->delete('posts');
        return TRUE;
    }
    
    function delete_range($start, $end){
        //delete range of posts ant their replies (if any)
        if (preg_match('/[^0-9]/', $start) == true) show_error('wtf bro');
        if (preg_match('/[^0-9]/', $end) == true) show_error('wtf bro');
        
        $i = $start;
	$t = $end + 1;
        while ($i<$t) {
            //delete posts
            $this->db->where('id', $i)->or_where('parent', $i)->delete('posts');
            $i++;
        }

        return TRUE;
    }
    
    function bump_thread($id) {
        // bump thread by id
        $data['bumped'] = time();
        $this->db->where('id', $id);
        $this->db->update('posts', $data);
        
        return TRUE;
    }
    
    function get_all_feed() {
        //get RSS of crypt0chan
        $this->db->from('boards');
        $this->db->join('posts', 'posts.boardid = boards.id');
        $this->db->where('visible', '1');
        $this->db->order_by('timestamp', 'desc');
        $this->db->limit(20);
        return $this->db->get();
    }
    
    function get_board_feed($boardid) {
        //get RSS of board
        $this->db->from('posts');
        $this->db->where('boardid', $boardid);
        $this->db->order_by('timestamp', 'desc');
        $this->db->limit(20);
        return $this->db->get();
    }

    function get_thread_feed($threadid) {
        //get RSS of thread
        //TO DO: no boardid anymore
        $this->db->from('posts');
        $this->db->where('id', $threadid);
        $this->db->or_where('parent', $threadid);
        $this->db->order_by('timestamp', 'desc');
        $this->db->limit(20);
        return $this->db->get();
    }
    
    function boardid_by_postid($id) {
        //will return just boardid of post
        $this->db->from('posts');
        $this->db->where('id', $id);
        return $this->db->get()->row()->boardid;
    }
    
    function last_reply_time($threadid) {
        //stats
        //will get last reply time of thread
        $today = date('d', time());
        $yesterday = date('d', time()) - 1;
        
        $this->db->from('posts');
        $this->db->where('parent', $threadid);
        $this->db->order_by('timestamp', 'desc');
        $this->db->limit(1);
        $result = $this->db->get()->row();
        
        
        if (date('m', time()) == date('m', $result->timestamp) && date('Y', time()) == date('Y', $result->timestamp ) && date('d', time()) == date('d', $result->timestamp )) {
                return "today at " . date('H:i', $result->timestamp);
        } else {
            return date("d.m.Y H:i", $result->timestamp);
        }
     
    }
    

    function count_replies($id) {
        //stats
        //return number of replies in thread
        $this->db->from('posts');
        $this->db->where('parent', $id);
        return $this->db->get()->num_rows();
    }
    
    public function check_messages($time) {
        // return number of posts since time
        if (preg_match('/[^0-9]/', $time) == true) {
            show_error('what are you doing, nigga???');
        }
        $this->db->from('boards');
        $this->db->join('posts', 'posts.boardid = boards.id');
        $this->db->where('visible', '1');
        $this->db->where('timestamp >', $time);
        $this->db->limit(51);
        return $this->db->get()->num_rows();
    }
    
    function live($number) {
        //return live posts
        if (preg_match('/[^0-9]/', $number) == true) show_error('what are you doing, nigga???');
        if ($number < 1 || $number > 50) show_error('only 1-50 messages allowed');
        
        $this->db->select('posts.id, parent, timestamp, message, boardname');
        $this->db->from('boards');
        $this->db->join('posts', 'posts.boardid = boards.id');
        $this->db->where('visible', '1');
        $this->db->order_by('timestamp', 'desc');
        $this->db->limit($number);
        return $this->db->get();
    }
}