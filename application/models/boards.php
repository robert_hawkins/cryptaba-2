<?php

class Boards extends CI_Model {
 
    function valid_board_name($boardname) {
        //if board exists - return true
        $this->db->from('boards');
        $this->db->where('boardname', $boardname);
        $result = $this->db->get();
        
        if ($result->num_rows() > 0) {
            return TRUE;  
        } else {
            show_error('invalid board name!');
        }
    }
    
    function valid_board_id($boardid) {
        //if board exists - return true
        $this->db->from('boards');
        $this->db->where('id', $boardid);
        $result = $this->db->get();
        
        if ($result->num_rows() > 0) {
            return TRUE;  
        } else {
            show_error('invalid board id!');
        }
    }
    
    function id_by_name($boardname) {
        $this->db->from('boards');
        $this->db->where('boardname', $boardname);
        $this->db->select('id');
        return $this->db->get()->row()->id;
    }
    
    function name_by_id($boardid) {
        $this->db->from('boards');
        $this->db->where('id', $boardid);
        $this->db->select('boardname');
        return $this->db->get()->row()->boardname;
    }
    
    function board_info($boardid) {
        //return any board info and stats, such as: number of threads, posts, irc info, motd, etc.
        
        //get number of threads in board 
        $this->db->from('posts');
        $this->db->where('parent', '0');
        $this->db->where('boardid', $boardid);
        $threads_num = $this->db->get()->num_rows();
        //get number of posts in board
        $this->db->from('posts');
        $this->db->where('boardid', $boardid);
        $posts_num = $this->db->get()->num_rows();
        //get descryption, irc, motd
        $this->db->from('boards');
        $this->db->where('id', $boardid);
        $result = $this->db->get()->row();
        $boarddescr = $result->boarddescr;
        $irc = $result->irc;
        $motd = $result->motd;
        
        //return results as array
        return array(
            'threads_num' => $threads_num,
            'posts_num' => $posts_num,
            'boarddescr' => $boarddescr,
            'irc' => $irc,
            'motd' => $motd
        );
    }
    
    function visible_boards_info() {
        // get all visible boards (for menu, stats, etc)
        $this->db->from('boards');
        $this->db->where('visible', 1);
        $this->db->order_by('boardname');
        return $this->db->get();
    }
    
    function overall_stats() {
        //main page statistics
        
        //get total posts number
        $this->db->from('posts');
        $totalposts = $this->db->get()->num_rows();
        //get total threads number
        $this->db->from('posts');
        $this->db->where('parent', 0);
        $totalthreads = $this->db->get()->num_rows();
        
        return array(
            'totalposts' => $totalposts,
            'totalthreads' => $totalthreads
        );
    }
    
    function add_board($data) {
        //add a board
        $clean_data = array(
            'boardname' => $data['name'],
            'visible' => $data['visible'],
            'boarddescr' => $data['descr'],
            'adminid' => '1'
        );        
        $this->db->insert('boards', $clean_data);
        return true;
    }
    
    function change_motd($data) {
        //change motd of board
        $clean_data = array('motd' => $data['motd']);
        $this->db->where('boardname', $data['name'])->update('boards', $clean_data);
        return true;
    }
    
    function delete_board($id) {
        $this->db->where('boardid', $id)->delete('posts');
        $this->db->where('id', $id)->delete('boards');
    }
    
}

