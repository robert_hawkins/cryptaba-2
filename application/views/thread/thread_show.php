<div class="container"> <!-- start content -->
    <div class="stats-board">
        <h2>/<?= $boardname ?>/ — thread  #<?= $threadid ?></h2>
    </div> 
    <div class="stats-board-stuff">
        <span class="stats-info"><b>Replies:</b> <?= $this->posts->count_replies($threadid) ?></span>
        <span class="stats-rss">
            <a class="rss-link" href="<?= rel_url('feed/thread' . "/" . $threadid) ?>">
                RSS
            </a>
        </span>
    </div>

    <? foreach ($posts_id->result() as $row) { ?>
        <div class="reply <? if ($row->parent == 0) echo " op"; ?>" id="n<?= $row->id ?>">
            <a name="id<?= $row->id ?>"></a>
            <div class="reply-header">  
                <a href="#" onclick="quickReply('<?= $row->id ?>');">
                    <b>#<?= $row->id ?> at <?= date("D d M Y H:i:s", $row->timestamp) ?></b>
                </a>
                <? if ($row->timestamp > $this->input->cookie('lastvisit')): ?>
                    <span class="wut">new</span>
                <? endif; ?>
            </div>
            <div class="reply-message">
                <?= $this->messages->markup($row->message) ?>
            </div>
        </div>
        <?
    } # End foreach loop
    ?>
</div> <!--end content -->
