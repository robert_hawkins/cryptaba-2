<nav class="navigation">
    
    <span class="navigation-addition">
        <? if (isset($posts_id)): ?>
            <span class="navigation-left-item"><?= $this->anchors->board_anchor($boardname, '[ go back ]') ?></span>
        <? endif; ?>
    </span>

    <span class="navigation-main">
        <span class="muted">boards:</span>
        <?
        foreach ($visible_boards->result() as $row):
            echo '<span class="navigation-right-item">';
            echo $this->anchors->board_anchor($row->boardname, "[ " . $row->boardname . " ] ");
            echo '</span>';
        endforeach;
        ?>
        <span class="navigation-right-item"><a href="<?= rel_url() ?>">[ cd ~ ]</a></span>
    </span>

</nav>
