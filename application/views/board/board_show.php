<div class="container"> <!-- start content -->
    <div class="stats-board">
        <h2>/<?= $boardname ?>/ — <?= $board_info['boarddescr'] ?></h2>
        <? if ($board_info['motd'] != ""): ?>
            <p><?= $board_info['motd'] ?></p>
        <? endif; ?>
    </div>
    <div class="stats-board-stuff">
        <span class="stats-info">
            <b>Threads:</b> <?= $board_info['threads_num'] ?>,
            <b>Posts:</b> <?= $board_info['posts_num'] ?>,

            <? if ($board_info['irc'] != "") : ?>
                <b>IRC channel:</b> #<?= $board_info['irc'] ?>
            <? endif; ?>
        </span>
        <span class="stats-rss">
            <a class="rss-link" href="<?= rel_url('feed/board/' . $boardname) ?>">
                RSS
            </a>
        </span>
    </div>

    <? foreach ($posts_index->result() as $row):
        $replies_count = $this->posts->count_replies($row->id);
        ?>
        <div class="thread" id="n<?= $row->id ?>">
            <div class="thread-header">
                <b>
                    <?= $this->anchors->thread_anchor($boardname, $row->id, 
                            'Thread #' . $row->id . ' at ' . date("D d M Y H:i:s", $row->timestamp))
                    ?>                    
                </b>

                <span class="muted">
                    (Replies: <?= $replies_count ?>) 
                    <? if ($this->posts->count_replies($row->id) > 0): ?>
                        (Last reply: <?= $this->posts->last_reply_time($row->id) ?>)
                    <? endif; ?>
                </span>

                <? if ($row->timestamp > $this->input->cookie('lastvisit')): ?>
                    <span class="label label-success">new</span>
                <? endif; ?>
            </div><br>

            <div class="thread-message">
                <?= $this->messages->markup($row->message) ?>
            </div>


            <div class="recent-replies">
                <? $recent_messages = $this->db->query('select * from (select * from posts where parent=' .
                        $row->id . ' order by id desc limit 3) order by id asc'); 
                if ($replies_count - $recent_messages->num_rows() > 0): ?>
                    <div class="ommited"><?= ($replies_count - 3) ?> posts ommited.</div>
                <? endif; ?>
    <? foreach ($recent_messages->result() as $rm): ?>
                    <div class="recent-reply" id="n<?= $rm->id ?>">
                        <a name="id<?= $rm->id ?>"></a>
                        <div class="reply-header">  
                            <a href="<?= rel_url('trd/' . $row->id . '#n' . $rm->id) ?>">
                                <b>#<?= $rm->id ?> at <?= date("D d M Y H:i:s", $rm->timestamp) ?></b>
                            </a>
                            <? if ($rm->timestamp > $this->input->cookie('lastvisit')): ?>
                                <span class="wut">new</span>
                            <? endif; ?>
                        </div>
                        <div class="reply-message">
                            <?= $this->messages->markup($rm->message) ?>
                        </div>
                    </div>
    <? endforeach; ?>
            </div>
        </div>
<? endforeach; ?>

    <div class="pagination">
<?= $pagination ?>
    </div>

</div> <!--end content -->
