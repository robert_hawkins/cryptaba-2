

<div class="container"> <!-- start content -->
<div class="stats-board">
    <h1> Latest posts </h1>
    <button class="btn" id="lastvisit-markread">✓ Clear</button>
    <button class="btn" id="lastvisit-reloadposts"><i class="icon-refresh icon-white"></i>↻ Reload</button>
</div>
    <div id="new-posts-container"></div>
</div> <!--end content -->


<script type="text/javascript">
    $('#lastvisit-reloadposts').click(function(){ 
        fillNewMessages();
    });
    
    $('#lastvisit-markread').click(function(){ 
        markRead();
            setTimeout(function(){
                fillNewMessages();
        }, 1000);
    });
    fillNewMessages();

</script>