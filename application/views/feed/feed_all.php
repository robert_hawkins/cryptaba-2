<?=header('Content-Type: text/xml');?>

<rss version="2.0">
    <channel>
        <title>
            Last posts on crypt0chan
        </title>
        <description>Last updates and activity via RSS. Stay in touch, anon!</description>
        <link><?=site_url()?></link>
<?
    foreach ($feed->result() as $row) {
?>
        <item>
            <title>
<?
    if ($row->parent != 0) {
        echo $row->boardname . ": Reply to #" . $row->parent . " (" . $row->id . ")";
    } else {
        echo $row->boardname . ": New thread #" . $row->id;
    }
?>    
            </title>
            <link>
<?
    if ($row->parent != 0) {
        echo site_url('trd/' . $row->parent. '#n' . $row->id);
    } else {
        echo site_url('trd/' . $row->id);
    }
?>
            </link>
            <description><![CDATA[<?=$this->messages->clean_markup($row->message)?>]]></description>
            <pubDate><?=date("r" ,$row->timestamp)?></pubDate>
        </item>
<?
    }
?>
    </channel>
</rss>
