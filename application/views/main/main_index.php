<div class="container container-home">
    <!-- start content -->
    <div class="home-banner">  
        <div class="home-banner-image">
            <img src="<?= rel_base_url('img/logo.png'); ?>" class="home-logo-image">
            <div class="home-banner-message"><?= $this->config->item('site_descr') ?></div>
        </div>
    </div>

    <div class="black-well">

        <div class="home-well">
            <div class="home-header">Choose board:</div>
            <div class="home-boards-list">
            <? foreach ($visible_boards->result() as $row): ?>
                <div class="home-boards-list-item">
                    <?=
                    $this->anchors->board_anchor($row->boardname, "/" . $row->boardname . "/") ." — " . $row->boarddescr;
                    ?>
                </div>
            <? endforeach; ?>                         
            </div>
        </div>

        <div class="home-recent-messages home-well">
            <div class="home-recent-header">
                <span style="float: left;">Recent posts:</span>
                <span style="float: right;"><a class="rss-link" href="<?= rel_url('feed/all') ?>">RSS</a></span>
            </div>
            
            <div class="home-recent-messages-block" id="home-recent-messages">
                <noscript>Javascript is disabled. Recent messages not shown.</noscript>
            </div>
        </div>

        <div class="home-menu home-well">

            <div class="home-stats">
                <div class="home-header">Info:</div>
                <? /* <a href="<?= rel_url('main/site_settings') ?>"><h4>[ site settings ]</h4></a> */ ?>

                <p>Total posts: <?= $overall_stats['totalposts'] ?></p>
                <p>Total threads: <?= $overall_stats['totalthreads'] ?></p>
                <a href="<?= rel_url('main/main_help') ?>"> ~ Help ~</a>
            </div>

        </div>

    </div>

</div> 
<!-- end content -->

<script>
    setTimeout(function() {
        fillRecentMessages();
    }, 100);
</script>
