<div class="container container-home">


    <div class="">
        <h1>Global rules:</h1>
        <hr>
        <p>1. Any porn is forbidden.</p>
        <p>2. Administration is not responsible for content posted by users.</p>
    </div>

    <div class="">
        <h1>Anonymity:</h1>
        <p>No personal information about users stored in database if you are not registered and logged in.</p>
        <p>However, web server can write logs. You must cloak your IP addres, useragent and other system info if you want to be completely anonymous.</p>
        <p><b>What information about you server can see:</b></p>
        <p>IP: <?= $this->input->server('REMOTE_ADDR'); ?> (use tor or i2p to cloak it for free)</p>
        <p>User Agent: <?= $this->input->server('HTTP_USER_AGENT'); ?> (google for useragent changing methods)</p>
        <p><b>Recommended settings:</b></p>
        <p>IP: 127.0.0.1 (if using tor or i2p gates)</p>
        <p>User Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36</p>
    </div>

    <div class="">
        <h1>Markup:</h1>
        <hr>
        <p>**bold** - <b>bold</b></p>
        <p>*italic* - <i>italic</i></p>
        <p>%%spolier%% - <span class="message-spoiler">spoiler</span></p>
        <p>&gt;quote - <span class="message-quote">&gt;quote</span></p>
        <p>`code();` - <pre class="prettyprint">code();</pre></p>
        <hr>
        <p>To post images: upload your image to imgur.com or tinypic.com, copy link (example: http://i.imgur.com/3FVlh0X.gif) and simply paste it in your message.<br>
            Image rendering can be configured in <b>Global site settings</b></p>
    </div>
</div>
