<div class="container">
    <h1>Global site settings</h1>
    <hr>
        <form class="form-actions" action="<?=rel_url('main/site_settings')?>" method="POST">
            Load images from imgur/tinypic?<br>
            <select name="set_images">
                <option value="no">no</option>
                <option value="yes" <? if ($this->input->cookie('load_images') == '1') echo "selected"?>>yes</option>
            </select><br>
            
            Load youtube videos?<br>
            <select name="set_youtube">
                <option value="no">no</option>
                <option value="yes" <? if ($this->input->cookie('load_youtube') == '1') echo "selected"?>>yes</option>
            </select><br>
            <button type="submit" class="btn btn-success">save</button>
        </form>
</div>

