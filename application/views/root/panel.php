<html>
    <head>
        <title>R00t C0ntr0ll3r</title>
        <style>
            body {
                background: #000;
                color: #0c0;
                font-family: monospace;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>r00t panel</h1>
            <div><a href="<?=rel_url('root/logout')?>">[ logout ]</a></div>
            <hr>
            <div>
                <form action="<?=rel_url('root/del_post');?>" method="POST" id="root-delpost">
                    <p>del post (by id):</p>
                    <input type="text" name="postid">
                    <input type="submit" value="delete">
                </form>                
            </div>
            
            <hr>
            <div>
                <form action="<?=rel_url('root/del_post_range');?>" method="POST" id="root-delpostrange">
                    <p>del posts range:</p>
                    from <input type="text" name="id_start" style="width: 50px;">
                     to <input type="text" name="id_end" style="width: 50px;">
                    <input type="submit" value="delete">
                </form>                
            </div>

            <hr>
            <div>
                <form action="<?=rel_url('root/add_board');?>" method="POST" id="root-addboard">
                    <p>add new board:</p>
                    board name <input type="text" name="name">
                    board description <input type="text" name="descr">
                    visible <input type="text" name="visible" value="1">
                    <input type="submit" value="add">
                </form>                
            </div>

            <hr>
            <div>
                <form action="<?=rel_url('root/delete_board');?>" method="POST" id="root-delete">
                    <p>delete board:</p>
                    board name <input type="text" name="name">
                    <input type="submit" value="delete">
                </form>                
            </div>
            
            <hr>
            <div>
                <form action="<?=rel_url('root/change_motd');?>" method="POST" id="root-motd">
                    <p>change motd:</p>
                    board name <input type="text" name="name">
                    motd <input type="text" name="motd">
                    <input type="submit" value="change">
                </form>                
            </div>
            
        </div>
    </body>
</html>