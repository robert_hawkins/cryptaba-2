<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <title><?= $this->config->item('site_name') ?></title>
        
        <link href="<?= rel_base_url(); ?>css/core.css" rel="stylesheet" type="text/css">
        <link href="<?= rel_base_url(); ?>css/crypt0.css" rel="alternate stylesheet" type="text/css" title="crypt0chan">
        <link href="<?= rel_base_url(); ?>css/terminal.css" rel="stylesheet" type="text/css" title="1337">
        <link href="<?= rel_base_url(); ?>css/photon.css" rel="alternate stylesheet" type="text/css" title="photon">
        <link href="<?= rel_base_url(); ?>js/prettify/prettify.css" type="text/css" rel="stylesheet">
        <?
        if (isset($threadid)) {
            echo '<link href="' . rel_url('feed/thread') . '/' . $threadid . '" rel="alternate" title="' . $boardname . ' ' . $threadid . ' feed' . '" type="application/rss+xml" />';
        } elseif (isset($boardname)) {
            echo '<link href="' . rel_url('feed/board') . '/' . $boardname . '" rel="alternate" title="' . $boardname . ' feed' . '" type="application/rss+xml" />';
        } else {
            echo '<link href="' . rel_url('feed/all') . '" rel="alternate" title="RSS feed" type="application/rss+xml" />';
        }
        ?>
        <style>
            .home-banner {
                background-image: url('<?= rel_base_url('img/bg.gif') ?>');
            }
        </style>
        <script type="text/javascript">
            var rel_url = "<?= rel_base_url(); ?>";
            var site_name = "<?= $this->config->item('site_name') ?>";
        </script>
        <script type="text/javascript" src="<?= rel_base_url(); ?>js/jquery.js"></script>
        <script type="text/javascript" src="<?= rel_base_url(); ?>js/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?= rel_base_url(); ?>js/prettify/prettify.js"></script>
        <script type="text/javascript" src="<?= rel_base_url(); ?>js/aes.js"></script>
        <script type="text/javascript" src="<?= rel_base_url(); ?>js/crypt0.js"></script>
        <script type="text/javascript" src="<?= rel_base_url(); ?>js/crypt0.extra.js"></script>
    </head>

    <body>
        <a id="top"></a>
