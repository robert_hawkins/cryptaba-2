    
<? if (isset($boardid)): ?>   
    <? if (isset($threadid)): ?>
        <div class="textarea-toggle"><a onclick="togglePostform();" href="#" class="muted">[ Reply ]</a></div>
    <? else:?>
        <div class="textarea-toggle"><a onclick="togglePostform();" href="#" class="muted">[ New thread ]</a></div>
    <?endif;?>
<? endif; ?>

<div class="postform" id="postform">
    <form name="postform" action="<?=rel_url('api/insert_postform')?>" method="post">
        <input type="hidden" name="boardid" value="<?=$boardid?>">
        <input type="hidden" name="parent" value="<?
            if (isset($threadid)){
                echo $threadid;
            } else {
                echo "0";
            }
        ?>">
        <div class="markup-bar"></div>
        <textarea name="message" id="textfield" class="span12" rows="5"></textarea><br>
        <input class="btn" value="Post!" type="submit" id="submitButton"> <span class="muted">(ctrl + enter)</span>

    </form>
</div>
