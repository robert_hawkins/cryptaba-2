<?php

/**
 * Relative url
 *
 * Returns the relative url from your config file
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('rel_url'))
{
	function rel_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->item('rel_url') . $CI->config->item('index_page') . $uri;
	}
}

if ( ! function_exists('rel_base_url'))
{
	function rel_base_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->item('rel_url') . $uri;
	}
}

?>
