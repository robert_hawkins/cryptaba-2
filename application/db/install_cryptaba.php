<?
$db = new SQLite3('crypt0board.db');
chmod('crypt0board.db', 0666);

//create posts table
$db->query('create table if not exists posts (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	parent INTEGER NOT NULL,
	timestamp INTEGER NOT NULL,
	bumped INTEGER NOT NULL,
	boardid INTEGER NOT NULL,
	userid INTEGER NOT NULL,
	message TEXT NOT NULL
)');

//create boards table
$db->query('create table if not exists boards (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	boardname TEXT NOT NULL,
	boarddescr TEXT NOT NULL,
	visible INTEGER NOT NULL,
	adminid INTEGER NOT NULL,
	irc TEXT,
	motd TEXT 
)');

//create users table
$db->query('create table if not exists users (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	username TEXT NOT NULL,
	password TEXT NOT NULL,
	name TEXT NOT NULL 
)');

//insert data into database
$db->query('insert into boards ( boardname, boarddescr, visible, adminid, irc) values (
	"general", 
	"general crypt0chan discussions", 
	"1", 
	"2", 
	"crypt0chan"
)');

//$db->query('insert into boards values ("2", "talks", "random shit", "1", "2", "crypt0chan", "The stories and information posted here are artistic works of fiction and falsehood.<br>Only a fool would take anything posted here as fact." )');

?>
