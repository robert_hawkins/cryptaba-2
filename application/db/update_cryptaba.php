<?
$db = new SQLite3('crypt0board.db');

// Add indexes
$db->query('create index if not exists parentindex on posts(parent)');
$db->query('create index if not exists timeindex on posts(timestamp)');
$db->query('create index if not exists boardindex on posts(boardid)');

// Add default user anonymous
$db->query('insert into users (username,name,password) values ("anonymous","anonymous","294de3557d9d00b3d2d8a1e6aab028cf")');
